#!/bin/bash

set -e

export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive
aptInstall='apt-get install -y --no-install-recommends'

function removeUnusedFiles() {
    find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
    \) -exec rm -rf '{}' +
}

apt-get update

buildPackages="dirmngr make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncurses5-dev  libncursesw5-dev xz-utils tk-dev libmagic-dev libffi-dev"

$aptInstall $buildPackages

#install python
wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz"

mkdir -p /usr/src/python
tar -xJC /usr/src/python --strip-components=1 -f python.tar.xz
rm python.tar.xz

(
cd /usr/src/python || exit
gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"
./configure \
    --build="$gnuArch" \
    --enable-loadable-sqlite-extensions \
    --enable-shared \
    --with-system-expat \
    --with-system-ffi \
    --without-ensurepip
make -j "$(nproc)"
make install
ldconfig
)

rm -rf /usr/src/python
python3 --version

(
cd /usr/local/bin || exit
ln -s idle3 idle
ln -s pydoc3 pydoc
ln -s python3 python
ln -s python3-config python-config
)

#install pip
wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'
python get-pip.py --disable-pip-version-check --no-cache-dir "pip==18.1"
pip --version;
pip install --upgrade pip
rm -f get-pip.py


#remove build packages
apt-get purge -y $buildPackages
apt autoremove -y

$aptInstall wget gnupg default-mysql-client default-libmysqlclient-dev libldap2-dev libsasl2-dev gcc curl gettext libkrb5-dev

#postgres-client
#postgres package fix see https://github.com/debuerreotype/docker-debian-artifacts/issues/24
#mkdir -p /usr/share/man/man1
#mkdir -p /usr/share/man/man7
#curl -L https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
#echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
#$aptInstall libpq-dev postgresql postgresql-contrib

#mariadb-client
wget https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
chmod +x mariadb_repo_setup
./mariadb_repo_setup --mariadb-server-version="mariadb-10.5"
$aptInstall libmariadb3 libmariadb-dev

# geo libraries
$aptInstall binutils libproj-dev gdal-bin

#cleanup
apt-get clean
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
removeUnusedFiles

rm -rf /.build
