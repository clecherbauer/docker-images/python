FROM registry.gitlab.com/clecherbauer/docker-images/debian:bullseye

USER root

ENV PATH /usr/local/bin:$PATH
ENV LANG C.UTF-8
ENV GPG_KEY 0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D
ENV PYTHON_VERSION 3.8.3
ENV PYTHON_PIP_VERSION 20.1.1

COPY .build /.build
RUN /.build/build.sh

EXPOSE 80

CMD ["python3"]
